import { Component, OnInit } from '@angular/core';
import { FormControl,FormBuilder,FormGroup, Validators } from '@angular/forms';
import {LoginService} from './login.service';
import {Login} from './login';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  constructor(private formBuilder:FormBuilder,private loginService:LoginService) {
    this.loginForm = this.formBuilder.group({
      email:['',Validators.required],
      password:['',Validators.required]
    });
   }

  ngOnInit() {
  }

  login(){
    console.log(this.loginForm.value);
    let user:Login = {username:this.loginForm.value.email,password:this.loginForm.value.password};
    let payload:any = {
      data: {
        type: 'login',
        attributes:user
      }
    };

    console.log(payload);

    this.loginService.login(payload).subscribe((res:any)=>{
      console.log(res);
    },(err)=>{
      console.log(err);
    });
  }

}

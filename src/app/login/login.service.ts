import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http:HttpClient) { 
  }

  login(data:any):any{
    return this.http.post("https://hub.cobiro.com/v1/login",data);
  }
}
